/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import java.awt.FlowLayout;
import javax.swing.*;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJFrame {
    public static void main(String[] args) {
        JFrame f = new JFrame("JToolBar");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        JPanel panel = new JPanel();  
        panel.setLayout(new FlowLayout());  
        JLabel label = new JLabel("JFrame By Example");  
        JButton button = new JButton();  
        button.setText("Button");  
        panel.add(label);  
        panel.add(button);  
        f.add(panel);  
        
        f.setSize(200,300);
        f.setLocationRelativeTo(null);  
        f.setVisible(true);
    }
}
