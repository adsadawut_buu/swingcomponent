/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJSeparator {
    public static void main(String[] args) {
        JMenuItem i1,i2,i3,i4,i5;
        JFrame f = new JFrame("JMenuBar");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        f.setSize(300,300);
        f.setLayout(null);
        
        JMenuBar menubar = new JMenuBar();
        JMenu menu = new JMenu("Inventory");
        JMenu submenu = new JMenu("Add On");
        i1 = new JMenuItem("Straga Sword");
        i2 = new JMenuItem("Fouris Dagger");
        i3 = new JMenuItem("Revising");
        i4 = new JMenuItem("KaiYang");
        i5 = new JMenuItem("Rare Drop Rate");
        menu.add(i1);
        menu.addSeparator();
        menu.add(i2);
        menu.addSeparator();
        menu.add(submenu);
        submenu.add(i3);
        submenu.addSeparator();
        submenu.add(i4);
        submenu.addSeparator();
        submenu.add(i5);
        menubar.add(menu);
        
        f.setJMenuBar(menubar);
        f.setVisible(true);
    }
}
