/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import java.awt.*;  
import javax.swing.JFrame;  
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXDisplayingImage extends Canvas{
    public void paint(Graphics g) {  
  
        Toolkit t=Toolkit.getDefaultToolkit();  
        Image  i=t.getImage("milogo.png");  
        g.drawImage(i, 300,300,this);  
          
    }  
        public static void main(String[] args) {  
        EXDisplayingImage m=new EXDisplayingImage();  
        JFrame f = new JFrame("DisplayImage");  
        f.add(m);  
        f.setSize(300,300);  
        f.setVisible(true);  
    }  
}
