/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import java.awt.event.*;    
import java.awt.*;    
import javax.swing.*;     
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJColorChooser extends JFrame implements ActionListener{
    JButton b;
    Container c;    
    EXJColorChooser() {
        c = getContentPane();
        c.setLayout(new FlowLayout());
        b = new JButton("color");
        b.addActionListener(this);
        c.add(b);
    }

    public void actionPerformed(ActionEvent e) {
        Color initialcolor = Color.RED;
        Color color = JColorChooser.showDialog(this, "Select a color", initialcolor);
        c.setBackground(color);
    }
    public static void main(String[] args) {
        EXJColorChooser ch = new EXJColorChooser();
        ch.setSize(300, 300);
        ch.setVisible(true);
        ch.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
