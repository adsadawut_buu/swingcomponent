/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJLabel {
    public static void main(String[] args) {
        final JFrame frame = new JFrame("Exapple Button");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setSize(300,300);
        
        final JButton button = new JButton("Ok");
        button.setSize(50,20);
        button.setLocation(125,50);
        
        JTextField txt = new JTextField();
        txt.setSize(175,20);
        txt.setLocation(70,30);
        
        final JLabel lbl = new JLabel("Hello ");
        lbl.setSize(200,20);
        lbl.setLocation(50, 80);
        lbl.setBackground(Color.BLUE);
        lbl.setOpaque(true);
        
        button.addActionListener(new ActionListener(){  
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    String x = txt.getText();
                    lbl.setText("Hello "+x);
                    txt.setText("");  
                    
                }catch(Exception ex){System.out.println(ex);} 
                
                }
        
         });  
        
        
        frame.add(lbl);
        frame.add(txt);
        frame.add(button);
        frame.setVisible(true);
    }
}
