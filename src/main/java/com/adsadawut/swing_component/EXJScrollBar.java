/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJScrollBar {
    public static void main(String[] args) {
         JFrame frame = new JFrame("JScrollBar");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);  
        frame.setSize(300,300);
        
        JScrollBar sb = new JScrollBar();
        sb.setBounds(10,20,100,200);
        
        frame.add(sb);
        frame.setVisible(true);
        
    }
}
