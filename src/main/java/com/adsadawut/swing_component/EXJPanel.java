/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import java.awt.*;  
import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJPanel {
    public static void main(String[] args) {
        JFrame f = new JFrame("JPanel");
        f.setSize(300,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(null);
        
        JPanel pn = new JPanel();
        pn.setBounds(50,50,200,200);
        pn.setBackground(Color.red);
        
        JButton b1=new JButton("Button 1");     
        b1.setBounds(50,100,60,30);    
        b1.setBackground(Color.yellow);   
        
        JButton b2=new JButton("Button 2");   
        b2.setBounds(100,100,60,30);    
        b2.setBackground(Color.green);   
        pn.add(b1); 
        pn.add(b2);  
        f.add(pn);
        f.setVisible(true);
    }
}
