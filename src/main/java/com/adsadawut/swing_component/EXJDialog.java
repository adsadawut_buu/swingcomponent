/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;  
import java.awt.*;  
import java.awt.event.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJDialog {
    private static JDialog d;  
    EXJDialog() {  
        JFrame f= new JFrame();  
        d = new JDialog(f , "JDialog", true);  
        d.setLayout( new FlowLayout() );  
        JButton b = new JButton ("OK");  
        b.addActionListener ( new ActionListener()  
        {  
            public void actionPerformed( ActionEvent e )  
            {  
                EXJDialog.d.setVisible(false);  
            }  
        });  
        d.add( new JLabel ("Click button to continue."));  
        d.add(b);   
        d.setSize(300,300);    
        d.setVisible(true);  
    }  
    public static void main(String args[])  
    {  
        new EXJDialog();  
    }  
}
