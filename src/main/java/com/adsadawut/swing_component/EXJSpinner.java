/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJSpinner {
    public static void main(String[] args) {
        JFrame f = new JFrame("JSpinner");
        f.setSize(300,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(null);
        
        SpinnerModel value =  
             new SpinnerNumberModel(5, //initial value  
                -5, //minimum value  
                10, //maximum value  
                1); //step  
        JSpinner spinner = new JSpinner(value);
        spinner.setBounds(100, 100, 50, 20);
        
        f.add(spinner); 
        f.setVisible(true);
        
        
    }
}
