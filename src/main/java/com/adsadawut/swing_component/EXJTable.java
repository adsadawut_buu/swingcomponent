/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJTable {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JTable");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        
        String data[][] = {{"101", "Amit", "670000"},
        {"102", "Jai", "780000"},
        {"101", "Sachin", "700000"}};
        
        String column[] = {"ID","NAME","SALARY"};
        
        JTable ta = new JTable(data,column);
        ta.setBounds(0,0,300,300);
        JScrollPane sp=new JScrollPane(ta);    
        
        frame.add(sp);
        frame.setVisible(true);
    }
}
