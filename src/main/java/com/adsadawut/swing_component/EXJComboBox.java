/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJComboBox {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JComboBox");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setSize(300,300);
        
        String Milk[] = {"MilkCow","MilkCoat","MilkChicken"};
        JComboBox cb = new JComboBox(Milk);
        cb.setBounds(50,75,100,50);
        
        frame.add(cb);
        frame.setVisible(true);
    }
}
