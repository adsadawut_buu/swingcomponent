/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;  
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJSlider extends JFrame {
    public EXJSlider() {
        JSlider sl = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        sl.setMinorTickSpacing(2);
        sl.setMajorTickSpacing(10);
        sl.setPaintTicks(true);
        sl.setPaintLabels(true);

        JPanel panel = new JPanel();
        panel.add(sl);
        add(panel);
        setDefaultCloseOperation(this.EXIT_ON_CLOSE);
    }
    public static void main(String s[]) {  
        EXJSlider frame = new EXJSlider();
        frame.pack();
        frame.setVisible(true);
    }
}
