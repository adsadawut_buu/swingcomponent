/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.JEditorPane;  
import javax.swing.JFrame;  
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJEditorPane {
     JFrame f = null;  
     
    public static void main(String[] a) {  
        (new EXJEditorPane()).test();  
    }  
     private void test() {  
        f = new JFrame("JEditorPane");  
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
        f.setSize(300, 300);  
        JEditorPane myPane = new JEditorPane();  
        myPane.setContentType("text/plain");  
        myPane.setText("Sleeping is necessary for a healthy body."  
                + " But sleeping in unnecessary times may spoil our health, wealth and studies."  
                + " Doctors advise that the sleeping at improper timings may lead for obesity during the students days."); 
        f.setContentPane(myPane);  
        f.setVisible(true);  
    }  
}
