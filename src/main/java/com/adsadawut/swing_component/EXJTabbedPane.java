/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJTabbedPane {
    public static void main(String[] args) {
        JFrame f = new JFrame("JTabbedPane");
        f.setSize(300,300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setLayout(null);
        
        JTextArea ta =new JTextArea(100, 100);
        JPanel p1 = new JPanel();
        p1.add(ta);
        JPanel p2 = new JPanel();
        p2.add(ta);//ทดสอบว่าเพิ่มไปใน P2 ได้ไหม?
        JPanel p3 = new JPanel();
        p3.add(ta); //ทดสอบว่าเพิ่มไปใน P3 ได้ไหม?
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 100);
        tp.add("main", p1);
        tp.add("visit", p2);
        tp.add("help", p3);
        f.add(tp);
        
        f.setVisible(true);
    }
}
