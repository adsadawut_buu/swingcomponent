/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJCheckBox {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JPasswordField");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setSize(300,300);
        JCheckBox checkBox1 = new JCheckBox("YES");  
        checkBox1.setBounds(100,100, 50,50);  
        JCheckBox checkBox2 = new JCheckBox("No");  
        checkBox2.setBounds(100,150, 50,50);  
        
        frame.add(checkBox1);
        frame.add(checkBox2);
        frame.setVisible(true);
    }
}
