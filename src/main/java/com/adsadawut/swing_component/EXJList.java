/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJList {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JList");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);  
        frame.setSize(300,300);
        DefaultListModel<String> l1 = new DefaultListModel<>();  
          l1.addElement("Straga Sword");  
          l1.addElement("Fouris Dagger");  
          l1.addElement("Resirgur Wire");  
          l1.addElement("Vialto Saber");  
          JList<String> list = new JList<>(l1);  
          list.setBounds(100,100, 75,75);  
          
          frame.add(list);    
          
          frame.setVisible(true);  
        
    }
}
