/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import javax.swing.*;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJPasswordField {
    public static void main(String[] args) {
        JFrame frame = new JFrame("JPasswordField");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setSize(300,300);
        
        JLabel lbl = new JLabel("Password");
        lbl.setBounds(50,50,80,20);
        
        JPasswordField p =new JPasswordField();
        p.setBounds(120,45,100,30);
        
        frame.add(lbl);
        frame.add(p);
        frame.setVisible(true);
    }
}
