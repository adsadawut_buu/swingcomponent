/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.swing_component;

import java.awt.BorderLayout;  
import java.awt.Container;  
import javax.swing.*;
/**
 *
 * @author อัษฎาวุฒิ
 */
public class EXJToolBar {
    public static void main(String[] args) {
        JFrame f = new JFrame("JToolBar");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(300,300);
        
        JToolBar tb =new JToolBar();
        tb.setRollover(true);  
        JButton button = new JButton("File");  
        tb.add(button);  
        tb.addSeparator();  
        tb.add(new JButton("Edit"));  
        tb.add(new JComboBox(new String[] { "Opt-1", "Opt-2", "Opt-3", "Opt-4" }));  
        Container contentPane = f.getContentPane();  
        contentPane.add(tb, BorderLayout.NORTH);  
        JTextArea textArea = new JTextArea();  
        JScrollPane mypane = new JScrollPane(textArea);  
        contentPane.add(mypane, BorderLayout.EAST);  
        
        f.setVisible(true);
    }
}
